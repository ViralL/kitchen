'use strict';

// ready
$(document).ready(function() {

    //calc
    // input type ONLY number
    jQuery.fn.ForceNumericOnly = function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // sucess backspace, tab, delete, arrows, numbers
                return key == 8 || key == 9 || key == 46 || key == 190 || key >= 37 && key <= 40 || key >= 48 && key <= 57 || key >= 96 && key <= 105;
            });
        });
    };
    $(".number--js").ForceNumericOnly();
    // input type ONLY number
    function count_totals() {
        var summ = 0;
        var procent = 0;
        var countTotals = $('.calc-count--js').val() || 0;
        var firstSumm = $('.calc-firstcount--js').val() || 0;
        var period = $('.calc-period--js').val();
        var dateF = moment().add(period, 'month').format('DD.MM.YYYY');
        summ =  Math.ceil((parseFloat(countTotals) - parseFloat(firstSumm)) / period);
        procent = Math.ceil(parseFloat(firstSumm) * 100 / parseFloat(countTotals));
        if (!isNaN(summ) && summ >= 0) {
          $(".calc-fee--js").text(summ + ' p.');
          $(".calc-procent--js").text('Это ' + procent + '% от суммы покупки');
        } else {
          $(".calc-fee--js").text('Введите правильные значения');
        }
        $(".calc-countF--js").text(countTotals);
        $(".calc-periodF--js").text(period);
        $(".calc-dateF--js").text(dateF);
     }
     $(".number--js").on('keyup input', function() {
         count_totals();
     });
    //calc

    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $('.main-nav__collapse').toggleClass('active');
    });
    $('.footer-nav__item--title').click(function () {
        $(this).parent().toggleClass('active');
    });
    $('.show-more--js').click(function () {
        $(this).parent().hide().next().show('700');
        $('.slider-project').slick('setPosition');
        return false;
    });
    $('.accordion__link--js').click(function () {
        $(this).toggleClass('active').parent().next().slideToggle();
        return false;
    });
    $('.popup-close--js').click(function () {
        $(this).parent().fadeOut('700');
        return false;
    });
    // adaptive menu

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    $('.slideshow').slick({
      dots: true,
      dotsClass: 'slick-dots slick-dots-white',
      nextArrow: '<div class="slick-arrow-left slick-arrow"><i class="icon-slide-next"></i></div>',
      prevArrow: '<div class="slick-arrow-right slick-arrow"><i class="icon-slide-prev"></i></div>',
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            arrows: false
          }
        }
      ]
    });
    $('.slider').slick({
      dots: true,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      nextArrow: '<div class="slick-arrow-left slick-arrow slick-arrow-sl"><i class="icon-slide-next"></i></div>',
      prevArrow: '<div class="slick-arrow-right slick-arrow slick-arrow-sl"><i class="icon-slide-prev"></i></div>',
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
          }
        }
      ]
    });
    $('.slider-more').slick({
      dots: true,
      infinite: true,
      arrows: false,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 479,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]
    });
    $('.slider-project').slick({
      dots: true,
      arrows: false,
      adaptiveHeight: true,
      autoplay: true,
      dotsClass: 'slick-dots slick-dots-white slick-dots-mid'
    });
    $('.slider-project-autow').slick({
      dots: true,
      arrows: false,
      adaptiveHeight: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: true,
      variableWidth: true,
      dotsClass: 'slick-dots slick-dots-white slick-dots-mid',
      responsive: [
        {
          breakpoint: 479,
          settings: {
            variableWidth: false,
            slidesToShow: 1
          }
        }
      ]
    });
    $('.slider-project-b').slick({
      dots: true,
      arrows: false,
      adaptiveHeight: true,
      dotsClass: 'slick-dots slick-dots-bot'
    });
    $('.slider-project-bb').slick({
      dots: true,
      arrows: false,
      adaptiveHeight: true,
      dotsClass: 'slick-dots slick-dots-bbot'
    });
    $('.slider-notice').slick({
      nextArrow: '<div class="slick-arrow-left slick-arrow slick-arrow-lo"><i class="icon-slide-next"></i></div>',
      prevArrow: '<div class="slick-arrow-right slick-arrow slick-arrow-lo"><i class="icon-slide-prev"></i></div>',
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 479,
          settings: {
            dots: true,
            arrows: false
          }
        }
      ]
    });
    // slider

    // select {select2}
    $('select').select2({
      minimumResultsForSearch: Infinity
    });
    function formatState (state) {
      if (!state.id) { return state.text; }
      var $state = $(
        '<span><img src="../images/' + state.element.value.toLowerCase() + '.jpg" class="img-flag" /> ' + state.text + '</span>'
      );
      return $state;
    };
    $(".select-ico").select2({
      templateResult: formatState,
      templateSelection: formatState,
      minimumResultsForSearch: Infinity
    });
    // select

    // popup {magnific-popup}
    $('.popup').magnificPopup({
        showCloseBtn: false
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    // popup

    // input[type=file]
    $('.input__file-js').change(function() {
        $('.input__file-js').each(function() {
            var name = this.value;
            var reWin = /.*\\(.*)/;
            var fileTitle = name.replace(reWin, "$1");
            var reUnix = /.*\/(.*)/;
            fileTitle = fileTitle.replace(reUnix, "$1");
            $(this).parent().parent().find('.input__name-js').val(fileTitle);
            $(this).parent().find('.btn').text("Загружено");
        });
    });
    // input[type=file]

    // tooltip
    $('.masterTooltip').hover(function(){
      var title = $(this).attr('title');
      $(this).data('tipText', title).removeAttr('title');
      $('<p class="tooltip"></p>')
      .text(title)
      .appendTo('body')
      .fadeIn('slow');
    }, function() {
        $(this).attr('title', $(this).data('tipText'));
        $('.tooltip').remove();
    }).mousemove(function(e) {
        var mousex = e.pageX + 20;
        var mousey = e.pageY + 10;
        $('.tooltip')
        .css({ top: mousey, left: mousex })
    });
    // tooltip

});
// ready

$(window).on("load",function(){
    $(".objects-body-inner").mCustomScrollbar();
});

//map
function initMap() {
    var markers = [];
    var locations = [
        [59.91701049, 30.31812429],
        [59.91916157, 30.3251195],
        [59.91756978, 30.31812429],
        [59.92049517, 30.33250093],
        [59.91701049, 30.3276515]
    ];
    var hrefId = [
        'section1',
        'section2',
        'section3',
        'section4',
        'section5'
    ];

    var mapOptions = {
        center: new google.maps.LatLng(59.91916157, 30.3251195),
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        zoomControl: false,
        scrollwheel: false
    };
    map = new google.maps.Map(document.getElementById("map"), mapOptions);

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][0], locations[i][1]),
            map: map,
            icon: "../images/icons/bubble-a.png"
        });
        marker.set('data-href', hrefId[i]);
        markers.push(marker);
        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
            return function() {
                this.setIcon('../images/icons/bubble.png');
            }
        })(marker));
        google.maps.event.addListener(marker, 'mouseout', (function(marker) {
            return function() {
                this.setIcon('../images/icons/bubble-a.png');
            }
        })(marker));
        google.maps.event.addListener(marker, 'click', (function(marker) {
            return function() {
              var val = marker.get('data-href');
              $(".objects-body-inner").mCustomScrollbar("scrollTo", '#'+val);
              return false;
            }
        })(marker));
    }

    moveMarker( map, marker );
}
function initMap1() {
    var mapOptions = {
        center: new google.maps.LatLng(59.91916157, 30.3251195),
        zoom: 16,
        mapTypeControl: false,
        zoomControl: false,
        scrollwheel: false
    };
    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(59.91916157, 30.3251195),
        map: map,
        icon: "../images/icons/bubble-a.png"
    });

}

function moveMarker( map, marker ) {

    $('.objects-body__item').click(function () {
      var coords = $(this).data('adr');
      var latlngStr = coords.split(',', 2);
      var lat = parseFloat(latlngStr[0]);
      var lng = parseFloat(latlngStr[1]);

      marker.setPosition( new google.maps.LatLng( lat,  lng ) );
      map.panTo( new google.maps.LatLng( lat,  lng ) );
    });

};
//map

//
// // load
// $(document).load(function() {});
// // load
//
// // scroll
// $(window).scroll(function() {});
// // scroll
//
// // mobile sctipts
// var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
// if (screen_width <= 767) {}
// // mobile sctipts
